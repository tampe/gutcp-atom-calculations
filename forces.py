from constants import *
from math import sqrt

def fact(n):
    if(n == 0):
        return 1
    else:
        return n*fact(n-1)

def fele(Z,n,r):
    return (Z-n)*e**2/4./pi/eps0/r/r

def term1(l,m):
    return 1

def term2(l,m):
    return fact(l + abs(m))/(2*l+1)/float(fact(l - abs(m)))

ss = sqrt(3./4.)

def fdiam(i,l,m,r1,r2):
    q =  hbar**2 / 4. / me / r2**2 / r1 * ss
    if(i==1):
        return - term1(l,m) * q
    else:
        if(i==2):
            return - term2(l,m) * q    


def fdiam2(z,n,r1,r2):
    return -((z-n)/float(z-(n-1)))*(1-sqrt(2)/2.0)*r1*hbar**2/me/r2**4*10*ss


def fmag2(z,r1,r2):
    return 1/float(z)*hbar**2/me/r2**2/r1*ss


def radious2(Fele, Fdiag):
    a = hbar**2/me
    b = Fele + Fdiag
    print a,b,Fele,Fdiag
    return a / b

r1 = a0*(0.5 - sqrt(3./4.)/6.)

def r1(z):
    return a0*(1/float(z-1)-sqrt(3/4.)/float(z*(z-1)))

def r4(z):
    r = r1(z)
    tf = sqrt(3/4.)
    C = ((z-3)-(0.25-1/float(z))*tf*a0/r)
    A = (1-tf/float(z))/C
    D = A*A+4*((z-3)/float(z-2))*r/a0*10*tf/C
    return a0*(A + sqrt(D))/2

def rp1(z,n,a,b):
    r = r4(z)
    C = z-(n-1)-(a/8.-b/2./float(z))*sqrt(3)*a0/r
    A = 1/C
    D = A**2 + 20*sqrt(3)*(z-n)/float(z-(n-1))*(1-sqrt(2)/2.)*r/a0/C
    return a0*(A+sqrt(D))/2
    

def r10(z):
    return rp1(z,10.,5/3.,12.)

def r12(z):
    r = r10(z)
    C = z-11-(1/8.-3/float(z))*sqrt(3)*a0/r
    A = 1/C
    D = A**2 + 20*sqrt(3)*((z-12)/float(z-11))*(1+sqrt(2)/2.)*r/a0/C
    return a0*(A+sqrt(D))/2.

def rp2(z,n,a,b):
    r = r12(z)
    C = z -(n-1)-(a/8.-b/2./float(z))*sqrt(3)*a0/r
    A = 1/C
    D = A**2 + 20*sqrt(3)*(z-n)/float(z-(n-1))*(1 - sqrt(2)/2.+1/2.)*r/a0/C
    return a0*(A + sqrt(D))/2

def Eele(z,n,r):
    return (z-(n-1))*e**2/8./pi/eps0/r
    
a2 = [11/3., 7/3., 5/3., 4/3., 2/3., 1/3., 0]
b2 = [0,1,2,4,12,16,24,32,40]

a1 = [5/3]
b1 = [0,1,2,3,4,6,9,10,11,12,13,14]

z1 = [11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
n0 = 8
for b in b1:
    s  = 0
    ss = 0
    n  = 0
    for z in z1:
        r0 = rp1(z,n0, 5/3., 12)
        r  = rp1(z,n0, 5/3., b)
        e0 = Eele(z,n0,r0)
        en = Eele(z,n0,r)
        d = abs(e0-en)/abs(e0)
        #print z,b,e0,en,d
        s  += d
        ss += d*d
        n += 1

    print b, s/n, sqrt(ss/n - s*s/n/n)


eV = 6.2415093433e+18
for z in z1:
    ra1  = r1(z)
    ra4  = r4(z)
    ra10 = rp1(z,10, 5/3., 12)
    e0 = Eele(z,10,ra10)
    print z,ra1/a0,ra4/a0,ra10/a0,e0*eV

print (0.0259+0.0025+0.0058+0.0045+0.0023+0.0001+0.002+0.0038+0.0045+0.0054+0.0054+0.0078+0.0088+0.01+0.0086+0.0072+0.0111+0.0105+0.0068+0.0059) / 20.
 
"""
n=13
p2 = [1,1,1]
p3 = [1,0,0] - m[1,0,0]
==>  [1,1,1], 3*4 + (1 - 1)*4

n=14
p2 = [1,1,1}
p3 = [1,1,0] + m[0,0,1]
==>  [1,1,-1], 3*4 + (1+1-1)*4

n=15
p2 = [1,1,1]
p3 = [1,1,1]
==>  [0,0,0],  3*4 + (1+1+1)*4

n=16
p2 = [1,1,1]
p3 = [2,1,1] - m[1,0,0]
==> [-1,1,1], 3*4 +(2 + 1 + 1 - 1)*4

n=17
p2 = [1,1,1]
p3 = [2,2,1] + m[0,0,1]
==>  [0,0,1], 3*4 + (2 + 2 + 2 - 1)*4

n=18
p2 = [1,1,1]
p3 = [2,2,2]
==> [0,0,0], 3*4 + (2 + 2 + 2)*4 + 1*4

"""


z2 = [17,18,19,20,21,22,23,24,25,26,27,28,29,30]
em = [23.814,
      40.74,
      60.91,
      84.50,
      110.68,
      140.8,
      173.4,
      209.3,
      248.3,
      290.2,
      336.,
      384.,
      435.,
      490.]
n = 16
a = 0
b = 24.
for z,e0 in zip(z2,em):    
    ran1 = rp2(z,n,a,b)
    e01  = Eele(z,n,ran1)
    ran2 = rp2(z,n,1/3.,b)
    e02  = Eele(z,n,ran2)
    d1 = (e01*eV-e0)/e0
    d2 = (e02*eV-e0)/e0
    print z,e01*eV,d1,e02*eV,d2








